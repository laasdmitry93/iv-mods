# Мой сборник лучших модов для GTA IV

### 🙄 Что это за репозиторий и для чего он нужен?
Начнём с того, что данный репозиторий не имеет какого-то смысла для большинства людей и тем более разработчиков. <br/>
В первую очередь, он создан сугубо для себя любимого. <br/>
Теперь о его содержании: в данном репозитории будет два markdown-файла на русском и английском языках соответсвенно. В этих файлах будут записаны лучшие моды (с ссылками для скачивания) для GTA IV. <br/>
### 🤚 Внимание
> Важно учесть следующие критерии, по которым я отбирал моды
- Я старался подбирать максимально совместимые моды, которые не конфликтуют, не ломают игру, а исправляют, дополняют и улучшают её;
- Часть модов <b>нужно самому</b> "перелопатить" вручную, а не ставить сначала одну модификацию, а затем поверх неё, ещё одну (могут возникнуть конфликты и/или модификация(-ии) будет(-ут) работать некорректно или не работать вовсе);
- Никаких ENB и текстурных модов Вы тут не увидите, т.к. игра <b>очень капризна</b> даже на современном железе, потому что являтся 32-битной;
- Ниже перечисленные мною моды <b>Vanilla-Friendly</b>, а это значит, что все они вкупе будут отлично дополнять как сам сюжет игры, так и побочные действия, которые происходят вне основной сюжетной линии;
- <b>Рекомендую</b> использовать версию игры 1.0.8.0 (8-ой патч), т.к. эта версия является последннй модовосприимчивой. На крайний случай - <b>только</b> версию 1.0.7.0, <b>однако</b>, в ней больше недоработок, чем в версии 1.0.8.0.
> Кому нужен 8-ой патч (с русскими субитрами + EFLC), то вот ссылка для Вас - [тык](https://rutracker.org/forum/viewtopic.php?t=5453282)! <br/> 
Если нужен мультиплеер (далее MP) для основной игры и DLC, то скачиваем и устанавливаем [GTA Connected](https://gtaconnected.com), далее проделываем всё по [этой](https://gtaforums.com/topic/881495-launch-episodes-as-dlcs-in-gta-iv) инструкции! <br/> 
<b>P.S.</b> Если собираетесь играть в MP, то поставьте только эти моды: <b>FusionFix</b>, <b>Mirror Fix</b>, <b>Zolika1351's Trainer</b> и <b>IV Side-Activities</b>! <br/> 
Также <b>настоятельно рекомендую</b> иметь две версии игры: для одиночной игры и для MP. Для одичной версии <b>не обязательны DLC</b> и <b>можно устаналивать больше модов</b>. А для MP версии <b>лучше исполоьзовать DLC</b> и установить несколько модов (их названия привёл ранее). <br/> 
<b>P.P.S.</b> Фикс для радиостанций (решает проблему со смешиванием оснвных радиостанций с таковыми из DLC), если совместили DLC с основной игрой для игры в MP режиме - [тык](https://gtaforums.com/topic/962013-make-gta-iv-ignore-eflc-music/#comment-1071392910)

### 📃 Список модов (по категориям)
#### Исправления и улучшения:
- [FusionFix](https://github.com/ThirteenAG/GTAIV.EFLC.FusionFix/releases/tag/v1.10)
- [Mirror Fix](https://www.gtainside.com/en/gta4/mods/175771-mirror-fix)
- [Problem with water quality](https://gtaforums.com/topic/447305-problem-with-water-quality) - для версии игры 1.0.7.0 и ниже, а также для владельцев видеокарт AMD Radeon
- [Various Fixes](https://gtaforums.com/topic/975211-various-fixes)
- [Grand Theft Auto IV and Episodes From Liberty City: Characters Fixes](https://gtaforums.com/topic/927583-grand-theft-auto-iv-and-episodes-from-liberty-city-characters-fixes)
- [More Visible Interiors](https://gtaforums.com/topic/974099-more-visible-interiors)
- [Liberty Ferry Terminal - Waiting Room Fix](https://mega.nz/file/JnQFBa4C#iT0xK5CaC0mTs3oyDyp9SwWVYFLUHkFmZeyQzUj3pQw)
- [Rancher Collision Model Fix](https://www.gtainside.com/en/gta4/cars/124635-gta-4-rancher-collision-model-fix)
- [New Grass Height](https://www.gtainside.com/en/gta4/mods/176468-new-grass-height)
- [iCEnhancer 3.0 Natural](https://www.gtainside.com/en/gta4/mods/135841-icenhancer-3-0-natural) - отсюда нужно установить <b>только следующее</b>: "Map Detail Improvements", "Different Lightsprite options", "Reflective Glass for Busstops"
#### Оптимизация:
- [DXVK (Vulkan API)](https://github.com/doitsujin/dxvk/releases) - переопределяет вызовы DirectX на Vulkan API, тем самым повышая отрисовку кадров, а значит и плавность отображаемого контента (хорошо работает на AMD-картах, с Nvidia могут быть проблемы, но, это редкие случаи). Скачивать нужно последнюю актуальную версию. В основную папку игры нужно скопировать файл <b>d3d9.dll</b> (только один этот файл, <b>dxgi.dll и остальные файлы .dll</b> - копировать не нужно), который находится в папке <b>x32</b>!
#### Анимации
- [Alternate Fighting Animations](https://gtaforums.com/topic/984476-internet-robs-workshop/?do=findComment&comment=1072100556)
- [Beaten & Bruised](https://gtaforums.com/topic/979091-beaten-bruised)
- [Improved Animations Pack](https://gtaforums.com/topic/958625-improved-animations-pack)
#### Игровые скрипты и другие геймплейные улучшения
- [Bullet Penetration](https://gtaforums.com/topic/989496-bullet-penetration)
- [Liberty Rush](https://gtaforums.com/topic/979688-liberty-rush)
- [Potential Grim](https://gtaforums.com/topic/945227-iveflc-potential-grim)
- [Responsive Plus](https://gtaforums.com/topic/931069-iveflc-responsive-plus)
- [Various Pedestrian Actions](https://gtaforums.com/topic/976318-various-pedestrian-actions)
- [Project Birds](https://gtaforums.com/topic/980018-project-birds)
- [Plane Flight Path Improvements](https://www.gtagaming.com/plane-flight-path-improvements-f33591.html)
#### Графика
- [Sweet Autumn](https://gtaforums.com/topic/915206-iveflc-sweet-autumn)
#### Текстуры:
- [Dot Crosshair With HP (GTA IV + EFLC)](https://www.gtainside.com/en/gta4/mods/144131-dot-crosshair-with-hp-gta-iv-eflc)
- [Enhanced Improved Cop with YCHoP v5](https://www.lcpdfr.com/downloads/gta4mods/character/4251-enhanced-improved-cop-with-ychop)
- [Exaggerated blood v1.16](https://www.gtagaming.com/exaggerated-blood-v1-16-f2318.html)
- [HD Weapon Icons](https://www.gtainside.com/en/gta4/mods/159293-hd-weapon-icons)
- [Higher Res Radio Logos In-Game](https://gtaforums.com/topic/887527-ash_735s-workshop/page/3/#comment-1071559765)
- [Higher Res Radio Logos Menu](https://gtaforums.com/topic/887527-ash_735s-workshop/page/3/#comment-1071512871)
- [Improved Weapon Spec](https://gtaforums.com/topic/887527-ash_735s-workshop/page/4/#comment-1071652002)
- [No Watermarks In Exported Videos](https://www.gtagarage.com/mods/show.php?id=5867)
- [Vanilla friendly HD roads](https://www.gtainside.com/en/gta4/mods/186727-vanilla-friendly-hd-roads)
- [Vehicle Pack 1.7](https://gtaforums.com/topic/887527-ash_735s-workshop/page/2/#comment-1071451182)
- [Xbox One/Series S+X Buttons](https://gtaforums.com/topic/887527-ash_735s-workshop/page/4/#comment-1071669058)
#### Звуки:
> Три лучших пака звуков, по моему мнению, каждому на выбор
- [Sparks's GTA IV Weapon Sound Revamp v3.1](https://gtaforums.com/topic/974655-iv-sparkss-weapon-sound-revamp) 
- [Insurgency Gun Sounds](https://www.gtainside.com/en/gta4/mods/144849-insurgency-gun-sounds)
- [CS:GO Gun Sounds](https://www.gtainside.com/en/gta4/mods/150321-cs-go-gun-sounds) - больше походит на стандартные звуки оружий GTA IV
#### Скрипты .DotNet
- [Bank Account](https://www.gtagaming.com/bank-account-updated-10-13-13-f10259.html)
- [Death Music IV](https://gtaforums.com/topic/970263-death-music-iv)
- [Dialogue System](https://discord.com/invite/eGGcZ3h)
- [Left Shift Sprint Fix](https://www.gtainside.com/en/gta4/mods/122865-left-shift-sprint-fix)
- [OvertakingFix](https://www.gtagarage.com/mods/show.php?id=24682)
- [Project2DFX](https://github.com/ThirteenAG/III.VC.SA.IV.Project2DFX/releases/tag/v0.0) - для патча 1.0.8.0 и ниже только версия 4.3
- [Real Recoil](https://www.gtagaming.com/real-recoil-f30353.html)
- [Bullet Spread/Recoil Fix v1.1](https://www.gtagaming.com/bullet-spread-recoil-fix-v1-1-f29635.html)
- [Remove Weapons On Death V1](https://www.gtainside.com/en/gta4/mods/151340-remove-weapons-on-death-v1)
- [RIL.Budgeted](https://gtaforums.com/topic/744584-reliv-rilbudgeted-population-budget-adjustertaxi-bug-fix)
- [V Like Screaming](https://gtaforums.com/topic/980257-v-like-screaming)
- [ZolikaPatch](https://gtaforums.com/topic/955449-iv-zolikapatch)
- [Zolika1351's Trainer](https://gtaforums.com/topic/896795-1000-1080-zolika1351s-trainermod-menu-rewritten)
#### Дополнительные моды (по желанию)
- [Better Wardrobes](https://www.gtainside.com/en/gta4/mods/180180-better-wardrobes)
- [Customizable Niko](https://www.gtainside.com/en/gta4/skins/165768-customizable-niko)
- [Dualsense Controller UI](https://www.gtainside.com/en/gta4/mods/186964-dualsense-controller-ui)
- [Enemy Alert](https://www.gtagaming.com/enemy-alert-f28414.html)
- [Equip Gun Mod .net Version](https://www.gtagarage.com/mods/show.php?id=13570)
- [Revamped Combat](https://gtaforums.com/topic/979069-revamped-combat) - отсюда берём только текстуры для прицела (название прицела: Simple Plus)
- [Steam Achievements for 1.0.7.0 & 1.0.8.0](https://gtaforums.com/topic/957432-steam-achievements-for-1070-1080)
- [IV-Presence](https://www.gtainside.com/en/gta4/mods/169787-iv-presence)
- [IV Side-Activities](https://gtaforums.com/topic/976408-iv-side-activities)
- [No Cursor Escape](https://www.gtainside.com/en/gta4/mods/160019-no-cursor-escape)
- [NYPD 2014 SS2000 Rumbler Siren](https://www.lcpdfr.com/downloads/gta4mods/audio/6787-nypd-2014-ss2000-rumbler-siren-federal-signal-smart-siren)
- [openCamera](https://gtaforums.com/topic/547234-iveflcrelsrc-opencamera)
- [Project Thunder](https://gtaforums.com/topic/982902-project-thunder)
- [Weapon Weight](https://gtaforums.com/topic/475078-weapon-weight)
- [Video Games Friend Activity (VGFA)](https://www.gtainside.com/en/gta4/mods/161960-video-games-friend-activity-vgfa)
### 📄 Мой commandline.txt
> Если испульзуете DXVK, то пропишите в commandline.txt своё разрешение дисплея (ширина и высота). <br> Все параметры можно найти тут: [параметры commandline.txt (playground.ru)](https://www.playground.ru/gta_4/guide/grand_theft_auto_4_tehnicheskie_voprosy_parametry_commandline_txt-1202874)
```
-disableimposters
-fullspecaudio
-forcehighqualitymirrors
-forcer2vb
-frameLimit 144
-fullscreen
-norestrictions
-availablevidmem 8147
-nomemrestrict
-percentvidmem 100
-reservedApp 0
-notimefix
/high
```